<%-- 
    Document   : index.jsp
    Created on : Nov 14, 2016, 8:53:13 PM
    Author     : Strahinja
--%>

<%@include file="header.jsp" %>
		<!-- begin of the main_section -->
		<section id="main_section" class="alignLeft">
			<!-- begin of the  box -->
                        
			<div class="leftBox">
			<h3>${movie.title}</h3>
			  <img src="${pageContext.request.contextPath}/resources/images/${movie.photo}" width="93" height="95" alt="photo 1" class="left" />
			  <p><b>Price:</b> <b>$${movie.price}</b> &amp; eligible for FREE Super Saver<br> Shipping on orders over <b>$${movie.supersaver}</b>.</p><br><p><b>Availability:</b> Usually ships within 24 hours</p><br>
                          <form method="POST" action="${pageContext.request.contextPath}/addtocart">
                              <input type="hidden" name="id" value="${movie.id}">
                              <input type="number" name="quantity" min="1" value="1">
                              <input type="submit" value="Add To Cart" >
                          </form>
			  <div class="clear"></div>
			</div>
                        
			<!-- end of the  box -->
                        
		</section>
		<!-- end of the main_aside -->
                <%@include file="footer.jsp" %>