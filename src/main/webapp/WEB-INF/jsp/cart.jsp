<%-- 
    Document   : index.jsp
    Created on : Nov 14, 2016, 8:53:13 PM
    Author     : Strahinja
--%>

<%@include file="header.jsp" %>
		<!-- begin of the main_section -->
		<section id="main_section" class="alignLeft">
			<!-- begin of the  box -->
                        <c:forEach  items="${movies}" var="movie">
			<div class="leftBox">
                        <h3>${movie.title}</h3>
			  <img src="${pageContext.request.contextPath}/resources/images/${movie.photo}" width="93" height="95" alt="photo 1" class="left" />
			  <p><b>Price:</b> <b>$${movie.price}</b> &amp; eligible for FREE Super Saver<br> Shipping on orders over <b>$${movie.supersaver}</b>.</p><br><p><b>Availability:</b> Usually ships within 24 hours</p><br>
			  <p>Qauntity: ${movie.quantity}</p>
			  <div class="clear"></div>
			</div>
                        </c:forEach>
			<!-- end of the  box -->
                        <div><a class="leftBox" id="order_confirm" href="${pageContext.request.contextPath}/placeorder">CONFIRM ORDER</a>
                            
                        </div>
		</section>
		<!-- end of the main_aside -->
                <%@include file="footer.jsp" %>