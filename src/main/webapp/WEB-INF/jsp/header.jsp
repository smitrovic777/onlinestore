<%-- 
    Document   : header
    Created on : Nov 15, 2016, 12:13:45 PM
    Author     : Strahinja
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head lang="en">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
        <title>Movie Store</title>
    </head>
    <body>
	<!-- begin of the wrapper -->
	<div id="wrapper">
	<!-- begin of the main_header -->
		<header id="main_header">
			<div class="rightAlign" id="nav_bar"><a href="${pageContext.request.contextPath}/">Home</a> | <a href="${pageContext.request.contextPath}/cart">View Cart</a> | <a href="${pageContext.request.contextPath}/form">Login</a></div>
			<div class="leftAlign"><img src="${pageContext.request.contextPath}/resources/images/logo.jpg"></div>
		</header>
		<!-- end of the main_header -->
		<!-- begin of the searchBox -->
		<div id="searchBox">
			<form method="POST" action="${pageContext.request.contextPath}/search">
                            <input class="field" type="text" name="search_input" id="search_input" placeholder="Enter a movie title here" required>
				<select class="field" name="category" required>
                                    <option value="0">Category</option>
                                    <c:forEach items="${categories}" var="category">
                                        <option value="${category.id}">${category.name}</option>
                                    </c:forEach>
				</select>
				<input type="submit" value="Search" class="button">
			</form>
		</div>
		<!-- end of the searchBox -->
		<!-- end of the main_header -->
		<!-- begin of the main_aside -->
		<aside id="main_aside" class="alignLeft">
		
			<div id="aside_list_categories" class="alignLeft">
				<dl class="alignLeft">
                                    <dt>All Movie Categories</dt>
                                    <c:forEach items="${categories}" var="category">
                                    <dd><a class="aside_a" href="${pageContext.request.contextPath}/${category.id}">${category.name}</a></dd>
                                    </c:forEach>
				</dl>
			</div>
		
		</aside>
		<!-- end of the main_aside -->