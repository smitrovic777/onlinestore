<%-- 
    Document   : index.jsp
    Created on : Nov 14, 2016, 8:53:13 PM
    Author     : Strahinja
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head lang="en">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/forms.css">
        <title>Movie Store</title>
    </head>
    <body>
        <!-- begin of the wrapper -->
        <div id="wrapper">
            <!-- begin of the main_header -->
            <header id="main_header">
                <div class="rightAlign" id="nav_bar"><a href="${pageContext.request.contextPath}/">Home</a> | <a href="${pageContext.request.contextPath}/cart">View Cart</a> | <a href="${pageContext.request.contextPath}/form">Login</a></div>
                <div class="leftAlign"><img src="${pageContext.request.contextPath}/resources/images/logo.jpg"></div>
            </header>
            <!-- end of the main_header -->
            <!-- begin of the searchBox -->
            <div id="searchBox">

                <div id="default_link"><a href="${pageContext.request.contextPath}/categories">Categories</a> | <a href="${pageContext.request.contextPath}/products">Products</a> | <a href="${pageContext.request.contextPath}/orders">Orders</a></div>

            </div>
            <!-- end of the searchBox -->
            <!-- end of the main_header -->
            <!-- begin of the main_aside -->
            <aside id="main_aside" class="alignLeft">

                <div id="aside_list_categories" class="alignLeft">
                    <dl class="alignLeft">
                        <dt>In production</dt>

                    </dl>
                </div>

            </aside>
            <!-- end of the main_aside -->

            <!-- begin of the main_section -->
            <section id="main_section" class="alignLeft">
                <div class="container">
                    <form enctype="multipart/form-data" id="generalform" method="POST" action="${pageContext.request.contextPath}/addmovietodb">
                        <label for="title">Title:</label>
                        <input type="text" class="input" name="title" placeholder="Enter Title here:" required>
                        <label for="price">Price:</label>
                        <input type="number" class="input" name="price" placeholder="Enter Price here:" required>
                        <label for="supersaver">SuperSaver:</label>
                        <input type="number" class="input" name="supersaver" placeholder="Enter SuperSaver here:" required>
                        <label for="title">Availability:</label>
                        <select class="input" name="availability">
                            <option value="1">True</option>
                            <option value="0">False</option>
                        </select>
                        <label for="category">Category:</label>
                        <select class="input" name="category">
                            <c:forEach items="${categories}" var="category">
                                <option value="${category.id}">${category.name}</option>
                            </c:forEach>
                        </select>
                        <label for="file">Photo</label>
                        <input class="input" type="file" name="file">
                        <input class="submit_btn" type="Submit" value="Submit">
                    </form>
                </div>
            </section>
            <!-- end of the main_aside -->
            <%@include file="../footer.jsp" %>