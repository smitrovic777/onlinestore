<%-- 
    Document   : index.jsp
    Created on : Nov 14, 2016, 8:53:13 PM
    Author     : Strahinja
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head lang="en">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/forms.css">
        <title>Movie Store</title>
    </head>
    <body>
        <!-- begin of the wrapper -->
        <div id="wrapper">
            <!-- begin of the main_header -->
            <header id="main_header">
                <div class="rightAlign" id="nav_bar"><a href="${pageContext.request.contextPath}/logout">Logout</a></div>
                <div class="leftAlign"><img src="${pageContext.request.contextPath}/resources/images/logo.jpg"></div>
            </header>
            <!-- end of the main_header -->
            <!-- begin of the searchBox -->
            <div id="searchBox">
                <div id="default_link"><a href="${pageContext.request.contextPath}/categories">Categories</a> | <a href="${pageContext.request.contextPath}/products">Products</a> | <a href="${pageContext.request.contextPath}/orders">Orders</a></div>
            </div>
            <!-- end of the searchBox -->
            <!-- end of the main_header -->
            <!-- begin of the main_aside -->
            <aside id="main_aside" class="alignLeft">

                <div id="aside_list_categories" class="alignLeft">
                    <dl class="alignLeft">
                        <dt>In Production</dt>

                    </dl>
                </div>

            </aside>
            <!-- end of the main_aside -->
            <!-- begin of the main_section -->
            <section id="main_section" class="alignLeft">
                <!-- begin of the  box -->

                <div>
                    <table id="default">
                        <tr>
                            <th>
                                ID
                            </th>
                            <th>
                                Category
                            </th>
                            <th>
                                Actions
                            </th>
                        </tr>
                        <c:forEach items="${categories}" var="category">
                            <tr>
                            <td>${category.id}</td>
                            <td> | ${category.name} | </td>
                            <td><button id="btn"><a  href="${pageContext.request.contextPath}/deletecategory/${category.id}">Delete</a></button><button id="btn"><a  href="${pageContext.request.contextPath}/updatecategory/${category.id}">Update</a></button></td>
                            </tr>
                        </c:forEach>

                    </table>
                    <button><a href="${pageContext.request.contextPath}/addcategory">Add</a></button>
                </div>

                <!-- end of the  box -->
                ${error}

            </section>
            <!-- end of the main_aside -->
            <%@include file="../footer.jsp" %>