<%-- 
    Document   : index.jsp
    Created on : Nov 14, 2016, 8:53:13 PM
    Author     : Strahinja
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head lang="en">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/forms.css">
        <title>Movie Store</title>
    </head>
    <body>
        <!-- begin of the wrapper -->
        <div id="wrapper_login">
            <div class="container">
            <form id="generalform" method="POST" action="${pageContext.request.contextPath}/login">
                <label for="firstname">Username:</label>
                <input type="text" class="input" name="username" placeholder="Enter your Username" required>
                <label for="lastname">Password:</label>
                <input type="text" class="input" name="password" placeholder="Enter your Password" required>
                <input class="submit_btn" type="Submit" value="Submit">
            </form>
            </div>
        </div>

    </body>
</html>