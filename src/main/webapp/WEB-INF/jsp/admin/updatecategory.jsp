<%-- 
    Document   : index.jsp
    Created on : Nov 14, 2016, 8:53:13 PM
    Author     : Strahinja
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head lang="en">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/forms.css">
        <title>Movie Store</title>
    </head>
    <body>
        <!-- begin of the wrapper -->
        <div id="wrapper">
            <!-- begin of the main_header -->
            <header id="main_header">
                <div class="rightAlign" id="nav_bar"><a href="${pageContext.request.contextPath}/">Home</a> | <a href="${pageContext.request.contextPath}/cart">View Cart</a> | <a href="${pageContext.request.contextPath}/form">Login</a></div>
                <div class="leftAlign"><img src="${pageContext.request.contextPath}/resources/images/logo.jpg"></div>
            </header>
            <!-- end of the main_header -->
            <!-- begin of the searchBox -->
            <div id="searchBox">
                
                    <div id="default_link"><a href="${pageContext.request.contextPath}/categories">Categories</a> | <a href="${pageContext.request.contextPath}/products">Products</a> | <a href="${pageContext.request.contextPath}/orders">Orders</a></div>
                
            </div>
            <!-- end of the searchBox -->
            <!-- end of the main_header -->
            <!-- begin of the main_aside -->
            <aside id="main_aside" class="alignLeft">

                <div id="aside_list_categories" class="alignLeft">
                    <dl class="alignLeft">
                        <dt>In production</dt>
                        
                    </dl>
                </div>

            </aside>
            <!-- end of the main_aside -->

            <!-- begin of the main_section -->
            <section id="main_section" class="alignLeft">
                <div class="container">
                    <form id="generalform" method="POST" action="${pageContext.request.contextPath}/updatecategorytodb">
                        <label for="firstname">Name:</label>
                        <input type="text" class="input" name="name" placeholder="Enter category Name here:" required>
                        <input type="hidden" name="id" value="${id}">
                        <input class="submit_btn" type="Submit" value="Submit">
                    </form>
                </div>
            </section>
            <!-- end of the main_aside -->
            <%@include file="../footer.jsp" %>