/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.strahinja.onlinestore.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 *
 * @author Strahinja
 */
@Entity
@Table(name = "orders")
@NamedQueries({
    @NamedQuery(name = "Orders.findAll", query = "SELECT o FROM Orders o"),
    @NamedQuery(name = "Orders.findById", query = "SELECT o FROM Orders o WHERE o.id = :id"),
    @NamedQuery(name = "Orders.findByUserFirstname", query = "SELECT o FROM Orders o WHERE o.userFirstname = :userFirstname"),
    @NamedQuery(name = "Orders.findByUserLastname", query = "SELECT o FROM Orders o WHERE o.userLastname = :userLastname"),
    @NamedQuery(name = "Orders.findByUserCountry", query = "SELECT o FROM Orders o WHERE o.userCountry = :userCountry"),
    @NamedQuery(name = "Orders.findByUserAddress", query = "SELECT o FROM Orders o WHERE o.userAddress = :userAddress"),
    @NamedQuery(name = "Orders.findByOrdertime", query = "SELECT o FROM Orders o WHERE o.ordertime = :ordertime"),
    @NamedQuery(name = "Orders.findByProducts", query = "SELECT o FROM Orders o WHERE o.products = :products")})
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 512)
    @Column(name = "user_firstname")
    private String userFirstname;
    @Size(max = 512)
    @Column(name = "user_lastname")
    private String userLastname;
    @Column(name = "user_country")
    private Integer userCountry;
    @Size(max = 512)
    @Column(name = "user_address")
    private String userAddress;
    @Column(name = "ordertime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ordertime;
    @Size(max = 1024)
    @Column(name = "products")
    private String products;
    @Transient
    public List<Movie> movies;
    @Transient
    public String movies_string;
    public Orders() {
    }

    public Orders(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserFirstname() {
        return userFirstname;
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
    }

    public Integer getUserCountry() {
        return userCountry;
    }

    public void setUserCountry(Integer userCountry) {
        this.userCountry = userCountry;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public Date getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(Date ordertime) {
        this.ordertime = ordertime;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orders)) {
            return false;
        }
        Orders other = (Orders) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "com.strahinja.onlinestore.model.Orders[ id=" + id + " ]";
    }
    
    public static String cleanIncomingString(String incoming_string) {
        incoming_string = incoming_string.replace("[{", "");
        incoming_string = incoming_string.replace("}]", "");
        incoming_string = incoming_string.replace("},{", "#");
        return incoming_string;
    }

    public static Movie parseProducts(String incoming_string) {
        Movie movie = new Movie();
        String[] movie_data = incoming_string.split(",");
        Integer id = Integer.parseInt(movie_data[0].split(":")[1]);
        Integer quantity = Integer.parseInt(movie_data[1].split(":")[1]);
        movie.setId(id);
        movie.setQuantity(quantity);
        return movie;
    }

    /**
     * @return the movies
     */
    public List<Movie> getMovies() {
        return movies;
    }

    /**
     * @param movies the movies to set
     */
    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    /**
     * @return the movies_string
     */
    public String getMovies_string() {
        return movies_string;
    }

    /**
     * @param movies_string the movies_string to set
     */
    public void setMovies_string(String movies_string) {
        this.movies_string = movies_string;
    }
    
}
