/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.strahinja.onlinestore.model;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Strahinja
 */
@Component
@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
public class MovieDao {

    @Autowired
    SessionFactory sessionFactory;

    public List<Movie> getAll() {
        Session session = sessionFactory.getCurrentSession();
        List<Movie> result = session.createCriteria(Movie.class).list();
        return result;
    }

    public List<Movie> getByCategory(Integer category) {
        Session session = sessionFactory.getCurrentSession();
        List<Movie> result = session.getNamedQuery("Movie.findByCategory").setInteger("category", category).list();
        return result;
    }

    public List<Movie> getByTitleAndCategory(Integer category, String search_input) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Movie.class);
        criteria.add(Restrictions.eq("category", category));
        criteria.add(Restrictions.like("title", "%" + search_input + "%"));
        List<Movie> result = criteria.list();
        return result;
    }

    public Movie getMovieById(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        Movie result = (Movie) session.get(Movie.class, id);
        return result;
    }

    public boolean deleteMovie(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("DELETE Movie WHERE id = :id").setParameter("id", id).executeUpdate();
        return true;
    }

    public boolean saveMovie(Movie m) {
        Session session = sessionFactory.getCurrentSession();
        session.save(m);
        return true;
    }

    public boolean updateMovie(Movie movie) {
        Session session = sessionFactory.getCurrentSession();
        session.update(movie);
        /*
        Query query = session.createQuery("UPDATE Movie set title=:title,price=:price,supersaver=:supersaver,availability=:av,category=:category WHERE id=:id");
        query.setParameter("price", movie.getPrice());
        query.setParameter("title", movie.getTitle());
        query.setParameter("supersaver", movie.getSupersaver());
        query.setParameter("av", movie.getAvailability());
        query.setParameter("category", movie.getCategory());
        query.setParameter("id", movie.getId());
        query.executeUpdate();
         */
        return true;
    }
}
