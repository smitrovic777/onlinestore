/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.strahinja.onlinestore.model;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Strahinja
 */
@Component
@Transactional(propagation =  Propagation.REQUIRED, readOnly = false)
public class OrdersDao {

    @Autowired
    SessionFactory sessionFactory;

    public boolean insertOrder(Orders o) {
        Session session = sessionFactory.getCurrentSession();
        session.save(o);
        return true;
    }
    
    public List<Orders> getAll(){
        Session session = sessionFactory.getCurrentSession();
        List<Orders> result = session.getNamedQuery("Orders.findAll").list();
        return result;
    }
    
    public boolean delete(Integer id){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("DELETE Orders WHERE id=:id").setParameter("id", id);
        query.executeUpdate();
        return true;
    }
}
