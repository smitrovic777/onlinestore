/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.strahinja.onlinestore.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import javax.validation.constraints.Size;

/**
 *
 * @author Strahinja
 */
@Entity
@Table(name = "movies")
@NamedQueries({
    @NamedQuery(name = "Movie.findAll", query = "SELECT m FROM Movie m"),
    @NamedQuery(name = "Movie.findById", query = "SELECT m FROM Movie m WHERE m.id = :id"),
    @NamedQuery(name = "Movie.findByTitle", query = "SELECT m FROM Movie m WHERE m.title = :title"),
    @NamedQuery(name = "Movie.findByPrice", query = "SELECT m FROM Movie m WHERE m.price = :price"),
    @NamedQuery(name = "Movie.findBySupersaver", query = "SELECT m FROM Movie m WHERE m.supersaver = :supersaver"),
    @NamedQuery(name = "Movie.findByAvailability", query = "SELECT m FROM Movie m WHERE m.availability = :availability"),
    @NamedQuery(name = "Movie.findByPhoto", query = "SELECT m FROM Movie m WHERE m.photo = :photo"),
    @NamedQuery(name = "Movie.findByCategory", query = "SELECT m FROM Movie m WHERE m.category = :category")})
public class Movie implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 1024)
    @Column(name = "title")
    private String title;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "supersaver")
    private BigDecimal supersaver;
    @Column(name = "availability")
    private Short availability;
    @Size(max = 1024)
    @Column(name = "photo")
    private String photo;
    @Column(name = "category")
    private Integer category;
    @Transient
    public Integer quantity = 0;
    
    public Movie() {
    }

    public Movie(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getSupersaver() {
        return supersaver;
    }

    public void setSupersaver(BigDecimal supersaver) {
        this.supersaver = supersaver;
    }

    public Short getAvailability() {
        return availability;
    }

    public void setAvailability(Short availability) {
        this.availability = availability;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }
    
    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
   
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

   
    public boolean equals(Object object) {
        if(object==null)return false;
        if (!(object instanceof Movie)) {
            return false;
        }
        Movie other = (Movie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }else if((this.title == null && other.title != null) || (this.title != null && !this.title.equals(other.title))){
            return false;
        }
        
        return true;
    }

    
    public String toString() {
        return "ID: "+this.getId()+" TITLE: "+this.getTitle()+" PRICE: "+this.getPrice();
    }

    
    
}
