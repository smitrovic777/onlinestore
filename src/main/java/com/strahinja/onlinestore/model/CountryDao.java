/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.strahinja.onlinestore.model;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Strahinja
 */
@Component
@Transactional(propagation = Propagation.REQUIRED,readOnly = false)
public class CountryDao {
    
    @Autowired
    SessionFactory sessionFactory;
    
    public List<Country> getAll(){
        Session session = sessionFactory.getCurrentSession();
        List<Country> result = session.createCriteria(Country.class).list();
        return result;
    }
    
}
