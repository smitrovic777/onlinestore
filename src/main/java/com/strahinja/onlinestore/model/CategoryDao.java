/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.strahinja.onlinestore.model;

import java.util.List;
import org.hibernate.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Strahinja
 */
@Component
@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
public class CategoryDao {

    @Autowired
    SessionFactory sessionFactory;

    public List<Category> getAll() {
        Session session = sessionFactory.getCurrentSession();
        List<Category> result = session.getNamedQuery("Category.findAll").list();
        return result;
    }

    public boolean deleteCategory(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("DELETE Category WHERE id = :id");
        query.setParameter("id", id);
        query.executeUpdate();
        return true;

    }
    
    public boolean insertCategory(Category c) {
        Session session = sessionFactory.getCurrentSession();
        session.save(c);
        return true;
    }
    
    public boolean update(Category category) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("UPDATE Category set name = :name WHERE id = :id");
        query.setParameter("name", category.getName());
        query.setParameter("id", category.getId());
        query.executeUpdate();
        return true;
    }
}
