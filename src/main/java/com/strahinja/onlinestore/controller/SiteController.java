/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.strahinja.onlinestore.controller;

import com.strahinja.onlinestore.model.Category;
import java.util.*;
import com.strahinja.onlinestore.model.CategoryDao;
import com.strahinja.onlinestore.model.Country;
import com.strahinja.onlinestore.model.CountryDao;
import com.strahinja.onlinestore.model.Movie;
import com.strahinja.onlinestore.model.MovieDao;
import com.strahinja.onlinestore.model.Orders;
import com.strahinja.onlinestore.model.OrdersDao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Strahinja
 */
@Controller
public class SiteController {

    @Autowired
    CategoryDao categoryDao;

    @Autowired
    MovieDao movieDao;

    @Autowired
    CountryDao countryDao;

    @Autowired
    OrdersDao ordersDao;

    @RequestMapping("/")
    public String index(ModelMap model) {
        List<Category> result = categoryDao.getAll();
        model.addAttribute("categories", result);
        List<Movie> resultMovies = movieDao.getByCategory(1);
        model.addAttribute("movies", resultMovies);
        return "index";
    }

    @RequestMapping("/{id}")
    public String indexWithCategory(@PathVariable Integer id, ModelMap model) {
        List<Category> result = categoryDao.getAll();
        model.addAttribute("categories", result);
        List<Movie> resultMovies = movieDao.getByCategory(id);
        model.addAttribute("movies", resultMovies);
        if (resultMovies.isEmpty()) {
            String error = "<div><span class='error'>Sorry we do not have movies of this category!</span></div>";
            model.addAttribute("error", error);
            return "index";
        }
        return "index";
    }

    @RequestMapping("/search")
    public String searchByTitle(@RequestParam(required = true) Integer category, @RequestParam(required = true) String search_input, ModelMap model) {
        if (category == 0) {
            List<Category> result = categoryDao.getAll();
            model.addAttribute("categories", result);
            String error = "<div><span class='error'>You have to select the Category, it can not be the default value!</span></div>";
            model.addAttribute("error", error);
            return "index";
        } else if ("".equals(search_input)) {
            List<Category> result = categoryDao.getAll();
            model.addAttribute("categories", result);
            String error = "<div><span class='error'>You have to enter the Movie Title!</span></div>";
            model.addAttribute("error", error);
            return "index";
        }
        search_input.trim();
        if (search_input.contains("<") || search_input.contains(">")) {
            List<Category> result = categoryDao.getAll();
            model.addAttribute("categories", result);
            String error = "<div><span class='error'>Please try to enter the Movie Title again!</span></div>";
            model.addAttribute("error", error);
            return "index";
        }
        List<Category> result = categoryDao.getAll();
        model.addAttribute("categories", result);
        List<Movie> resultMovies = movieDao.getByTitleAndCategory(category, search_input);
        model.addAttribute("movies", resultMovies);
        if (resultMovies.isEmpty()) {

            String error = "<div><span class='error'>No such Movie in our database!</span></div>";
            model.addAttribute("error", error);
            return "index";
        }
        return "index";
    }

    @RequestMapping("/tocart/{id}")
    public String addMovie(ModelMap model, @PathVariable Integer id) {
        Movie movie = movieDao.getMovieById(id);
        model.addAttribute("movie", movie);
        return "addtocart";
    }

    @RequestMapping("/addtocart")
    public String addMovieToCart(HttpServletRequest request, @RequestParam(required = true) Integer id, ModelMap model, @RequestParam(required = true) Integer quantity) {
        HttpSession session = request.getSession();
        HashMap<Integer, Movie> cart;
        if (session.getAttribute("cart") == null) {
            session.setAttribute("cart", new HashMap<Integer, Movie>());
        }
        cart = (HashMap<Integer, Movie>) session.getAttribute("cart");

        if (!cart.containsKey(id)) {
            Movie movie = movieDao.getMovieById(id);
            movie.setQuantity(quantity);
            cart.put(id, movie);
        } else {
            Movie movieFromCart = cart.get(id);
            movieFromCart.setQuantity(movieFromCart.getQuantity() + quantity);
        }
        String confirm = "<div><span class='confirm'>You succsessfuly added movie to cart!</span></div>";
        model.addAttribute("confirm", confirm);
        List<Category> categories = categoryDao.getAll();
        model.addAttribute("categories", categories);
        return "index";

    }

    @RequestMapping("/cart")
    public String cart(HttpServletRequest request, ModelMap model) {
        HttpSession session = request.getSession();
        HashMap<Integer, Movie> cart;
        cart = (HashMap<Integer, Movie>) session.getAttribute("cart");
        if (cart != null) {
            List<Movie> movies = new ArrayList<Movie>();
            if (!cart.isEmpty()) {
                for (Movie m : cart.values()) {
                    movies.add(m);
                }
            }
            model.addAttribute("movies", movies);
            return "cart";
        }
        List<Category> result = categoryDao.getAll();
        model.addAttribute("categories", result);
        String error = "<div><span class='error'>Your cart is empty!</span></div>";
        model.addAttribute("error", error);
        return "index";
    }

    @RequestMapping("/placeorder")
    public String placeOrder(ModelMap model) {
        List<Country> countries = countryDao.getAll();
        model.addAttribute("countries", countries);
        return "placeorder";
    }

    @RequestMapping("/order")
    public String order(HttpServletRequest request, @RequestParam(required = true) Integer country, @RequestParam(required = true) String address, @RequestParam(required = true) String lastname, @RequestParam(required = true) String firstname, ModelMap model) {
        HttpSession session = request.getSession();
        HashMap<Integer, Movie> cart;
        boolean result = false;
        if (session.getAttribute("cart") != null) {
            cart = (HashMap<Integer, Movie>) session.getAttribute("cart");
            StringBuilder movies = new StringBuilder();
            Orders order = new Orders();
            order.setUserFirstname(firstname);
            order.setUserLastname(lastname);
            order.setUserCountry(country);
            order.setUserAddress(address);
            order.setOrdertime(new java.sql.Date(new java.util.Date().getTime()));
            movies.append("[");
            for (Map.Entry<Integer, Movie> m : cart.entrySet()) {
                movies.append("{");
                movies.append("id:" + m.getValue().getId() + ",q:" + m.getValue().getQuantity() + "");
                movies.append("},");
            }
            movies.deleteCharAt(movies.length() - 1);
            movies.append("]");
            order.setProducts(movies.toString());
            result = ordersDao.insertOrder(order);
            session.removeAttribute("cart");
        }
        if (!result) {
            List<Category> categories = categoryDao.getAll();
            model.addAttribute("categories", categories);
            String error = "<div><span class='error'>Sorry the order was not sent,please try again!</span></div>";
            model.addAttribute("error", error);
            return "index";
        }
        List<Category> categories = categoryDao.getAll();
        model.addAttribute("categories", categories);
        String confirm = "<div><span class='confirm'>You succsessfuly placed a order!</span></div>";
        model.addAttribute("confirm", confirm);
        return "index";
    }
}
