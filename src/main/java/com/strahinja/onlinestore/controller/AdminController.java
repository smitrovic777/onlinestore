/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.strahinja.onlinestore.controller;

import com.strahinja.onlinestore.model.Category;
import com.strahinja.onlinestore.model.CategoryDao;
import com.strahinja.onlinestore.model.CountryDao;
import com.strahinja.onlinestore.model.Movie;
import com.strahinja.onlinestore.model.MovieDao;
import com.strahinja.onlinestore.model.Orders;
import com.strahinja.onlinestore.model.OrdersDao;
import com.strahinja.onlinestore.model.UserDao;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Strahinja
 */
@Controller
public class AdminController {

    @Autowired
    CategoryDao categoryDao;

    @Autowired
    MovieDao movieDao;

    @Autowired
    CountryDao countryDao;

    @Autowired
    OrdersDao ordersDao;

    @Autowired
    UserDao userDao;

    @RequestMapping("/form")
    public String form() {
        return "admin/login";
    }

    @RequestMapping("/login")
    public String login(HttpServletRequest request, @RequestParam(required = true) String password, @RequestParam(required = true) String username, ModelMap model) {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") == null) {
            String u = username.trim();
            String p = password.trim();
            boolean result = false;
            if ((!u.contains("<")) || (!u.contains(">")) || (!u.contains("\\")) || (!p.contains("<")) || (!p.contains("\\")) || (!p.contains(">"))) {
                result = userDao.userVerified(u, p);
            }
            if (result) {
                session.setAttribute("user", p);
                String confirm = "<div><span class='error'>Welcome to Managment System UI " + u + " !</span></div>";
                model.addAttribute("confirm", confirm);
                return "admin/main";
            }

            String error = "<div><span class='error'>Invalid user!</span></div>";
            model.addAttribute("error", error);
            return "index";
        }
        String u = (String) session.getAttribute("user");
        String confirm = "<div><span class='error'>Welcome to Managment System UI " + u + " !</span></div>";
        model.addAttribute("confirm", confirm);
        return "admin/main";
    }

    @RequestMapping("/logout")
    public String logout(HttpServletRequest request, ModelMap model) {
        HttpSession session = request.getSession();
        session.removeAttribute("user");
        List<Category> categories = categoryDao.getAll();
        model.addAttribute("categories", categories);
        List<Movie> resultMovies = movieDao.getByCategory(1);
        model.addAttribute("movies", resultMovies);
        return "index";
    }

    @RequestMapping("/categories")
    public String categories(ModelMap model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") != null) {
            List<Category> categories = categoryDao.getAll();
            model.addAttribute("categories", categories);
            return "admin/categories";
        }
        List<Category> categories = categoryDao.getAll();
        model.addAttribute("categories", categories);
        List<Movie> resultMovies = movieDao.getByCategory(1);
        model.addAttribute("movies", resultMovies);
        return "index";
    }

    @RequestMapping("/deletecategory/{id}")
    public String deleteCategory(HttpServletRequest request, @PathVariable Integer id, ModelMap model) {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") != null) {
            boolean result = categoryDao.deleteCategory(id);
            if (result) {
                List<Category> categories = categoryDao.getAll();
                model.addAttribute("categories", categories);
                return "admin/categories";
            } else {
                String error = "<div><span class='error'>Invalid user!</span></div>";
                model.addAttribute("error", error);
                List<Category> categories = categoryDao.getAll();
                model.addAttribute("categories", categories);
                return "admin/categories";
            }
        }
        List<Category> categories = categoryDao.getAll();
        model.addAttribute("categories", categories);
        List<Movie> resultMovies = movieDao.getByCategory(1);
        model.addAttribute("movies", resultMovies);
        return "index";
    }

    @RequestMapping("/addcategory")
    public String addCategory() {
        return "admin/addcategory";
    }

    @RequestMapping("/addcategorytodb")
    public String addCategoryToDb(@RequestParam(required = true) String name, ModelMap model) {
        Category c = new Category();
        c.setName(name);
        categoryDao.insertCategory(c);
        List<Category> categories = categoryDao.getAll();
        model.addAttribute("categories", categories);
        return "admin/categories";
    }

    @RequestMapping("/updatecategory/{id}")
    public String updateCategory(@PathVariable Integer id, ModelMap model) {
        model.addAttribute("id", id);
        return "admin/updatecategory";
    }

    @RequestMapping("/updatecategorytodb")
    public String updateCategoryToDb(@RequestParam(required = true) Integer id, ModelMap model, @RequestParam(required = true) String name) {
        Category c = new Category();
        c.setId(id);
        c.setName(name);
        categoryDao.update(c);
        List<Category> categories = categoryDao.getAll();
        model.addAttribute("categories", categories);
        return "admin/categories";
    }

    @RequestMapping("/products")
    public String products(ModelMap model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") != null) {
            List<Movie> movies = movieDao.getAll();
            model.addAttribute("movies", movies);
            return "admin/movies";
        }
        List<Category> categories = categoryDao.getAll();
        model.addAttribute("categories", categories);
        List<Movie> resultMovies = movieDao.getByCategory(1);
        model.addAttribute("movies", resultMovies);
        return "index";
    }

    @RequestMapping("/deletemovie/{id}")
    public String deleteMovie(HttpServletRequest request, @PathVariable Integer id, ModelMap model) {
        Movie m = movieDao.getMovieById(id);
        String path = request.getServletContext().getRealPath("/resources/images");
        File f = new File(path + "/" + m.getPhoto());
        f.delete();
        movieDao.deleteMovie(id);
        List<Movie> movies = movieDao.getAll();
        model.addAttribute("movies", movies);
        return "admin/movies";
    }

    @RequestMapping("/addmovie")
    public String addMovie(HttpServletRequest request, ModelMap model) {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") != null) {
            List<Category> categories = categoryDao.getAll();
            model.addAttribute("categories", categories);
            return "admin/addmovie";
        }
        List<Category> categories = categoryDao.getAll();
        model.addAttribute("categories", categories);
        List<Movie> resultMovies = movieDao.getByCategory(1);
        model.addAttribute("movies", resultMovies);
        return "index";
    }

    @RequestMapping("/addmovietodb")
    public String addMovieToDb(@RequestParam(required = true) Short availability, HttpServletRequest request, @RequestParam(required = true) MultipartFile file, @RequestParam(required = true) Integer category, @RequestParam(required = true) Double supersaver, @RequestParam(required = true) Double price, ModelMap model, @RequestParam(required = true) String title) throws IOException {
        Movie movie = new Movie();
        movie.setCategory(category);
        movie.setTitle(title);
        movie.setPrice(BigDecimal.valueOf(price));
        movie.setSupersaver(BigDecimal.valueOf(supersaver));
        movie.setAvailability(availability);
        String path = request.getServletContext().getRealPath("/resources/images");
        FileOutputStream fos = new FileOutputStream(path + "/" + file.getOriginalFilename());
        fos.write(file.getBytes());
        fos.close();
        movie.setPhoto(file.getOriginalFilename());
        movieDao.saveMovie(movie);
        List<Movie> movies = movieDao.getAll();
        model.addAttribute("movies", movies);
        return "admin/movies";
    }

    @RequestMapping("/updatemovie/{id}")
    public String updateMovie(HttpServletRequest request, ModelMap model, @PathVariable Integer id) {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") != null) {
            model.addAttribute("id", id);
            List<Category> categories = categoryDao.getAll();
            model.addAttribute("categories", categories);
            return "admin/updatemovie";
        }
        List<Category> categories = categoryDao.getAll();
        model.addAttribute("categories", categories);
        List<Movie> resultMovies = movieDao.getByCategory(1);
        model.addAttribute("movies", resultMovies);
        return "index";
    }

    @RequestMapping("/updatemovietodb")
    public String updateMovieToDb(@RequestParam(required = true) MultipartFile file, @RequestParam(required = true) Integer id, HttpServletRequest request, @RequestParam(required = true) Integer category, @RequestParam(required = true) Double supersaver, @RequestParam(required = true) Double price, ModelMap model, @RequestParam(required = true) String title) throws IOException {
        Movie m = movieDao.getMovieById(id);
        String path = request.getServletContext().getRealPath("/resources/images");
        File f = new File(path + "/" + m.getPhoto());
        f.delete();
        Movie movie = new Movie();
        movie.setId(id);
        movie.setCategory(category);
        movie.setTitle(title);
        movie.setPrice(BigDecimal.valueOf(price));
        movie.setSupersaver(BigDecimal.valueOf(supersaver));
        FileOutputStream fos = new FileOutputStream(path + "/" + file.getOriginalFilename());
        fos.write(file.getBytes());
        fos.close();
        movie.setPhoto(file.getOriginalFilename());
        movieDao.updateMovie(movie);
        List<Movie> movies = movieDao.getAll();
        model.addAttribute("movies", movies);
        return "admin/movies";
    }

    @RequestMapping("/orders")
    public String orders(HttpServletRequest request, ModelMap model) {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") != null) {
            List<Orders> orders = ordersDao.getAll();
            List<Movie> movies = movieDao.getAll();

            for (Orders o : orders) {
                List<Movie> orderMovies = new ArrayList<Movie>();
                String incoming_string = Orders.cleanIncomingString(o.getProducts());
                String[] clean_movies = incoming_string.split("#");
                for (int i = 0; i < clean_movies.length; i++) {
                    orderMovies.add(Orders.parseProducts(clean_movies[i]));
                }
                o.setMovies(orderMovies);
            }
            for (Orders o : orders) {
                for (int i = 0; i < o.getMovies().size(); i++) {
                    Movie o_movie = o.getMovies().get(i);
                    for (Movie m : movies) {
                        if (o_movie.getId() == m.getId()) {
                            o_movie.setTitle(m.getTitle());
                            o_movie.setPrice(m.getPrice());
                        }
                    }
                }
            }
            String m = null;
            for (Orders o : orders) {
                m = "";
                for (int i = 0; i < o.getMovies().size(); i++) {
                    m += o.getMovies().get(i).toString() + "\n";
                }
                o.setMovies_string(m);
            }
            model.addAttribute("orders", orders);
            return "admin/orders";
        }
        List<Category> categories = categoryDao.getAll();
        model.addAttribute("categories", categories);
        List<Movie> resultMovies = movieDao.getByCategory(1);
        model.addAttribute("movies", resultMovies);
        return "index";
    }
    
    @RequestMapping("/deleteorder/{id}")
    public String deleteOrder(ModelMap model, @PathVariable Integer id) {
        ordersDao.delete(id);

        List<Orders> orders = ordersDao.getAll();
        List<Movie> movies = movieDao.getAll();

        for (Orders o : orders) {
            List<Movie> orderMovies = new ArrayList<Movie>();
            String incoming_string = Orders.cleanIncomingString(o.getProducts());
            String[] clean_movies = incoming_string.split("#");
            for (int i = 0; i < clean_movies.length; i++) {
                orderMovies.add(Orders.parseProducts(clean_movies[i]));
            }
            o.setMovies(orderMovies);
        }
        for (Orders o : orders) {
            for (int i = 0; i < o.getMovies().size(); i++) {
                Movie o_movie = o.getMovies().get(i);
                for (Movie m : movies) {
                    if (o_movie.getId() == m.getId()) {
                        o_movie.setTitle(m.getTitle());
                        o_movie.setPrice(m.getPrice());
                    }
                }
            }
        }
        String m = null;
        for (Orders o : orders) {
            m = "";
            for (int i = 0; i < o.getMovies().size(); i++) {
                m += o.getMovies().get(i).toString() + "\n";
            }
            o.setMovies_string(m);
        }
        model.addAttribute("orders", orders);
        return "admin/orders";
    }
}
